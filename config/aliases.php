<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */
    'ContentAssetMgr' => Tecpro\CMS\Scripts\Managers\Facades\ContentAssetMgr::class,
    'ContentSlotMgr' => Tecpro\CMS\Scripts\Managers\Facades\ContentSlotMgr::class,
    'ContentCategoryMgr' => Tecpro\CMS\Scripts\Managers\Facades\ContentCategoryMgr::class,

];
