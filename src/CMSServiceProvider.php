<?php
namespace Tecpro\CMS;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class CMSServiceProvider extends ServiceProvider {
    public function boot() {
        $cmsConfig = config('cms');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadRoutes();
        $this->loadViewsFrom(__DIR__.'/resources/views', Arr::get($cmsConfig, 'namespace.view'));
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', Arr::get($cmsConfig, 'namespace.lang'));
        $this->loadSingletonFacade();
    }

    public function register() {
        $this->mergeConfigFrom(
            __DIR__.'/../config/aliases.php', 'app.aliases'
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/cms.php', 'cms'
        );
    }

    /**
     * Load all routes for administrator
     */
    protected function loadRoutes() {
        // Add prefix /admin for all URL
        $adminConfig = config('admin');
        Route::name(Arr::get($adminConfig, 'group.name'))
            ->prefix(Arr::get($adminConfig, 'group.prefix'))
            ->middleware('web')
            ->group(function () {
                $this->loadRoutesFrom(__DIR__.'/routes/cms.php');
            });
    }

    /**
     * Load App singleton facade
     */
    public function loadSingletonFacade() {
        $this->app->singleton('contentAssetMgr', function () {
            return new \Tecpro\CMS\Scripts\Managers\ContentAssetMgr();
        });

        $this->app->singleton('contentSlotMgr', function () {
            return new \Tecpro\CMS\Scripts\Managers\ContentSlotMgr();
        });

        $this->app->singleton('contentCategoryMgr', function () {
            return new \Tecpro\CMS\Scripts\Managers\ContentCategoryMgr();
        });
    }
}
