<?php

namespace Tecpro\CMS\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PDOException;
use Tecpro\CMS\App\Http\Requests\ContentCategoryFormRequest;
use Tecpro\CMS\Scripts\Managers\Facades\ContentCategoryMgr;

class ContentCategoryController extends Controller
{
    /**
     * Show the content category list
     * @param \Illuminate\Http\Request The HTTP request
     * @return \Illuminate\Contracts\View\View Render content category list view
     */
    public function showCategoryList(Request $request)
    {
        $parentId = $request->input('parentId');
        $contentCategoryPaging = ContentCategoryMgr::getSubCategoryOf($parentId, 10);

        return view('cms::category.categoryList', [
            'contentCategoryPaging' => $contentCategoryPaging,
            'parentId' => $parentId,
            'prevParentId' => ''
        ]);
    }

    /**
     * Show the content category form
     * @param \Illuminate\Http\Request The HTTP request
     * @return \Illuminate\Contracts\View\View Render content category form view
     */
    public function showCategoryForm(Request $request)
    {
        $id = $request->input('id');
        $parentId = $request->input('parentId');
        $localeId = $request->input('localeId') ?? 'vi';
        $category = null;

        if (isset($id) && !isset($parentId)) {
            $category = ContentCategoryMgr::get($id);

            if (!isset($category)) {
                abort(404, 'Asset not found');
            }
        }

        return view('cms::category.categoryForm', [
            'category' => isset($category) ? $category->transform($localeId) : null,
            'parentId' => $parentId
        ]);
    }

    /**
     * Create the content category and plug it to the parent category if it has. The ID will not be changed after creating
     * @param \Tecpro\CMS\App\Http\Requests\ContentCategoryFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function createCategory(ContentCategoryFormRequest $request)
    {
        $validated = $request->validated();
        $errorMessage = '';
        $validated['id'] = Str::slug($validated['id']);
        $validated['path'] = makePath($validated['id']);

        // Check parent category whether it existed. Throw error if it does not exist
        if (isset($validated['parent_id'])) {
            $parentCat = ContentCategoryMgr::get($validated['parent_id']);

            if (!isset($parentCat)) {
                return redirect()->back()->withErrors([
                    'errorMessage' => __('cms::error.category.parent.does.not.exist')
                ]);
            }

            $validated['path'] = makePath($parentCat->path . '/' . $validated['id']);
        }

        try {
            DB::beginTransaction();
            ContentCategoryMgr::create($validated);
            DB::commit();
        } catch (Error | PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            // Show error when failed
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            // Return the category form with the data
            return redirect()->route('admin.cms.category.form', [
                'id' => $validated['id']
            ]);
        }
    }

    /**
     * Update the content category
     * @param \Tecpro\CMS\App\Http\Requests\ContentCategoryFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function updateCategory(ContentCategoryFormRequest $request)
    {
        $validated = $request->validated();
        $errorMessage = '';
        $updatedCat = null;
        $currentCatId = Str::slug($request->input('prevId') ?? '');
        $currentCat = ContentCategoryMgr::get($currentCatId);

        if (!isset($currentCat)) {
            return redirect()->back()->withErrors([
                'errorMessage' => __('cms::error.category.does.not.exist')
            ]);
        }

        try {
            DB::beginTransaction();
            $updatedCat = ContentCategoryMgr::update($currentCatId, $validated);
            DB::commit();
        } catch (Error | PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            // Show error when failed
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            // Return the category form with the data
            return redirect()->route('admin.cms.category.form', [
                'id' => $updatedCat->id
            ]);
        }
    }
}
