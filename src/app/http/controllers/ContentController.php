<?php

namespace Tecpro\CMS\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PDOException;
use Tecpro\CMS\App\Http\Requests\ContentAssetFormRequest;
use Tecpro\CMS\App\Http\Requests\ContentSlotFormRequest;
use Tecpro\CMS\App\Models\ContentAsset;
use Tecpro\CMS\App\Models\ContentSlot;
use Tecpro\CMS\Scripts\Managers\Facades\ContentAssetMgr;
use Tecpro\CMS\Scripts\Managers\Facades\ContentSlotMgr;

class ContentController extends Controller
{
    /**
     * Show the content asset list
     * @return \Illuminate\Contracts\View\View Render content asset list view
     */
    public function showAssetList() {
        $contentAssetPaging = ContentAsset::paginate(10)->withQueryString();

        return view('cms::asset.assetList', [
            'contentAssetPaging' => $contentAssetPaging
        ]);
    }

    /**
     * Create the content asset
     * @param \Tecpro\CMS\App\Http\Requests\ContentAssetFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function createAsset(ContentAssetFormRequest $request) {
        $validated = $request->validated();
        $errorMessage = '';
        $newAsset = null;
        $validated['id'] = Str::slug($validated['id']);

        try {
            DB::beginTransaction();
            $newAsset = ContentAssetMgr::create($validated);
            DB::commit();
        } catch (Error|PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.cms.asset.form', [
                'id' => $newAsset->id
            ]);
        }
    }

    /**
     * Show the content asset form
     * @param \Illuminate\Http\Request The HTTP request
     * @return \Illuminate\Contracts\View\View Render content asset form view
     */
    public function showAssetForm(Request $request) {
        $id = $request->input('id');
        $localeId = $request->input('localeId') ?? 'vi';
        $contentAsset = null;

        if (isset($id)) {
            $contentAsset = ContentAssetMgr::get($id, $localeId);

            if (!isset($contentAsset)) {
                abort(404, 'Asset not found');
            }
        }

        return view('cms::asset.assetForm', [
            'contentAsset' => $contentAsset
        ]);
    }

    /**
     * Update the content asset
     * @param \Tecpro\CMS\App\Http\Requests\ContentAssetFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function updateAsset(ContentAssetFormRequest $request) {
        $validated = $request->validated();
        $prevId = $request->input('prevId');
        $errorMessage = '';
        $currentAsset = ContentAssetMgr::get($prevId, $validated['locale_id']);

        if (!isset($currentAsset)) {
            return redirect()->back()->withErrors([
                'errorMessage' => __('cms::content.asset.error.not.found')
            ]);
        }

        try {
            $validated['id'] = Str::slug($validated['id']);
            DB::beginTransaction();
            ContentAssetMgr::update($prevId, $validated);
            DB::commit();
        } catch (Error|PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.cms.asset.form', [
                'id' => $validated['id']
            ]);
        }
    }

    /**
     * Show the content slot list
     * @return \Illuminate\Contracts\View\View Render content slot list view
     */
    public function showSlotList() {
        $contentSlotPaging = ContentSlot::paginate(10)->withQueryString();

        return view('cms::slot.slotList', [
            'contentSlotPaging' => $contentSlotPaging
        ]);
    }

    /**
     * Show the content slot form
     * @param \Illuminate\Http\Request The HTTP request
     * @return \Illuminate\Contracts\View\View Render content slot form view
     */
    public function showSlotForm(Request $request) {
        $id = $request->input('id');
        $contentSlot = null;

        if (isset($id)) {
            $contentSlot = ContentSlotMgr::get($id);

            if (!isset($contentSlot)) {
                abort(404, 'Slot not found');
            }
        }

        return view('cms::slot.slotForm', [
            'contentSlot' => $contentSlot
        ]);
    }

    /**
     * Create the content slot
     * @param \Tecpro\CMS\App\Http\Requests\ContentSlotFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function createSlot(ContentSlotFormRequest $request) {
        $validated = $request->validated();
        $errorMessage = '';
        $newSlot = null;
        $validated['id'] = Str::slug($validated['id']);

        try {
            DB::beginTransaction();
            $newSlot = ContentSlotMgr::create($validated);
            DB::commit();
        } catch (Error|PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.cms.slot.form', [
                'id' => $newSlot->id
            ]);
        }
    }

    /**
     * Update the content slot
     * @param \Tecpro\CMS\App\Http\Requests\ContentSlotFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function updateSlot(ContentSlotFormRequest $request) {
        $validated = $request->validated();
        $prevId = $request->input('prevId');
        $errorMessage = '';
        $currentSlot = ContentSlotMgr::get($prevId);

        if (!isset($currentSlot)) {
            return redirect()->back()->withErrors([
                'errorMessage' => __('cms::content.asset.error.not.found')
            ]);
        }

        try {
            $validated['id'] = Str::slug($validated['id']);
            DB::beginTransaction();
            ContentSlotMgr::update($prevId, $validated);
            DB::commit();
        } catch (Error|PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.cms.slot.form', [
                'id' => $validated['id']
            ]);
        }
    }
}
