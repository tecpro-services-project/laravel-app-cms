<?php

namespace Tecpro\CMS\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentAssetFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|max:64',
            'is_enable' => '',
            'name' => 'max:128',
            'title' => 'max:128',
            'description' => 'max:1024',
            'keyword' => 'max:1024',
            'body' => 'max:16777215',
            'locale_id' => 'required|max:8',
        ];
    }
}
