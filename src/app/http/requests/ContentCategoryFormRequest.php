<?php

namespace Tecpro\CMS\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentCategoryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * content_category
             */
            'id' => 'required|max:32',
            'parent_id' => 'max:32',
            'path' => 'required|max:1024',

            /**
             * content_category_detail
             */
            'is_enable' => '',
            'name' => 'max:128',
            'title' => 'max:128',
            'description' => 'max:1024',
            'keyword' => 'max:1024',
            'locale_id' => 'required|max:8',
        ];
    }
}
