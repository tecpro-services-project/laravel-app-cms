<?php

namespace Tecpro\CMS\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentSlotFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|max:32',
            'is_enable' => '',
            'description' => 'max:256',
            'config' => 'max:65535',
        ];
    }
}
