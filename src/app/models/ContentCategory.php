<?php

namespace Tecpro\CMS\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class ContentCategory extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'content_category';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'parent_id',
        'path',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Link the category detail
     */
    public function detail() {
        return $this->hasMany(ContentCategoryDetail::class, 'id', 'id');
    }

    /**
     * Get new content category model
     * @return \Tecpro\CMS\App\Models\ContentCategoryDetail The content category model 
     */
    public function newDetail() {
        return new ContentCategoryDetail();
    }

    /**
     * Get the content category model
     * @param string $localeId The locale ID
     * @return \Tecpro\Ecommerce\App\Models\ContentCategoryDetail|null The content category model 
     */
    public function getDetailModel(string $localeId) {
        return $this->detail()->where('locale_id', $localeId)->first();
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '') {
        $final = $this->toArray();
        $endPoint = 'content/category/' . $this->id;
        $items = [];

        foreach ($this->items()->get() as $item) {
            array_push($items, $item->transform($localeId));
        }

        return array_merge($final, [
            'detail' => $this->getDetailModel($localeId)->transform(),
            'isAllowUpdateKey' => $this->isAllowUpdateKey,
            'endPoint' => $endPoint,
            'url' => url($endPoint),
            'items' => $items
        ]);
    }

    /**
     * Return category content category items relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Category content category items relation hasMany
     */
    public function items() {
        return $this->hasMany(ContentCategoryItem::class, 'category_id', 'id');
    }

    /**
     * Get new category content category item
     * @return \Tecpro\Ecommerce\App\Models\ContentCategoryItem The category content category item 
     */
    public function newItem() {
        return new ContentCategoryItem();
    }
}
