<?php

namespace Tecpro\CMS\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class ContentCategoryDetail extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'content_category_detail';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'is_enable',
        'name',
        'title',
        'description',
        'keyword',
        'locale_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     */
    public $incrementing = false;

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        return $this->toArray();
    }
}
