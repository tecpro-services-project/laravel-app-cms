<?php

namespace Tecpro\CMS\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class ContentCategoryItem extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'content_category_item';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'category_id',
        'content_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Return product relation hasOne
     * @return \Illuminate\Database\Eloquent\Relations\HasOne Product relation hasOne
     */
    public function content()
    {
        return $this->hasOne(ContentAsset::class, 'id', 'content_id');
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        $final = $this->toArray();
        $content = $this->content()->get()->first();

        return array_merge($final, [
            'content' => isset($content) ? $content->transform($localeId) : null
        ]);
    }
}
