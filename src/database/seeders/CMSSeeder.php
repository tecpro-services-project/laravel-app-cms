<?php

namespace Tecpro\CMS\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Tecpro\CMS\Scripts\Managers\Facades\ContentCategoryMgr;
use Tecpro\CMS\Scripts\Managers\Facades\ContentSlotMgr;

class CMSSeeder extends Seeder
{
    /**
     * @var string
     */
    private $rawDir;

    /**
     * @var \Illuminate\Support\Carbon
     */
    private $now;

    public function __construct()
    {
        $this->rawDir = makePath(__DIR__ . '/raw');

        // Date format must be: Y-m-d
        $this->now = Date::now();
    }

    /**
     * Scan and create a set of asset record array
     * @param string $path The directory path that contains asset file
     * @param string $fileExt The file extension name that needs to be removed. Then the result will be parsed to the asset ID, name, title, description
     * @return array The set of asset record array
     */
    public function scanAndCreateAssetRecord(string $path = '', string $fileExt = '.blade.php')
    {
        $rawFiles = scandir(makePath($path));
        $records = [];
        $currentDate = Date::now();

        foreach ($rawFiles as $fileName) {
            $filePath = makePath($path . '/' . $fileName, true);

            if (strcmp($fileName, '.') === 0 || strcmp($fileName, '..') === 0 || is_dir($filePath)) continue;

            $contentId = str_replace($fileExt, '', $fileName);
            $headline = Str::headline($contentId);

            array_push($records, [
                'id' => $contentId,
                'is_enable' => '1',
                'name' => $headline,
                'title' => $headline,
                'description' => $headline,
                'keyword' => $headline,
                'body' => file_get_contents($filePath),
                'locale_id' => 'vi',
                'created_at' => $currentDate,
                'updated_at' => $currentDate,
            ]);
        }

        return $records;
    }

    /**
     * Seed content assets
     */
    public function seedContentAssets()
    {
        $insert = [];
        $insert = array_merge($insert, $this->scanAndCreateAssetRecord($this->rawDir));
        $insert = array_merge($insert, $this->scanAndCreateAssetRecord($this->rawDir . '/html', '.html'));
        \Tecpro\CMS\App\Models\ContentAsset::insert($insert);
    }

    /**
     * Seed content slots
     */
    public function seedContentSlots()
    {
        $contentSlots = [
            [
                'id' => 'homepage-content',
                'is_enable' => '1',
                'description' => 'Homepage content slot',
                'config' => File::get(makePath($this->rawDir . '/slot/homepage-slot.txt', true))
            ],
            [
                'id' => 'homepage-banner',
                'is_enable' => '1',
                'description' => 'Homepage banner slot',
                'config' => File::get(makePath($this->rawDir . '/slot/homepage-banner.txt', true))
            ],
        ];

        foreach ($contentSlots as $slot) {
            ContentSlotMgr::create($slot);
        }
    }

    public function seedContentCategories()
    {
        $contentCats = [
            [
                'id' => 'blog',
                'path' => 'blog/'
            ],
            [
                'id' => 'event',
                'path' => 'event/'
            ],
            [
                'id' => 'workshop',
                'path' => 'workshop/'
            ],
            [
                'id' => 'service',
                'path' => 'service/'
            ]
        ];

        foreach ($contentCats as $cat) {
            $headLine = Str::headline($cat['id']);

            $cat['is_enable'] = 1;
            $cat['locale_id'] = 'vi';
            $cat['name'] = $headLine;
            $cat['title'] = $headLine;
            $cat['description'] = $headLine;
            $cat['keyword'] = $headLine;
            $cat['created_at'] = $this->now;
            $cat['updated_at'] = $this->now;

            ContentCategoryMgr::create($cat);
        }
    }

    public function seedContentCategoryItems()
    {
        $blogItems = [
            'ba-la-gi-blog',
            'cv-tester-blog',
            'ky-nang-mem-blog',
            'nghe-tester-blog',
            'pv-tester-blog',
            'tester-la-gi-blog',
            'trai-nganh-tester-blog',
        ];

        $eventItems = [
            'tester-3h-field-event',
            'tester-1000-salary-event',
            'testing-career-path-event',
        ];

        foreach ($blogItems as $item) {
            ContentCategoryMgr::createItem([
                'category_id' => 'blog',
                'content_id' => $item,
                'created_at' => $this->now,
                'updated_at' => $this->now
            ]);
        }

        foreach ($eventItems as $item) {
            ContentCategoryMgr::createItem([
                'category_id' => 'event',
                'content_id' => $item,
                'created_at' => $this->now,
                'updated_at' => $this->now
            ]);
        }
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->seedContentAssets();

        $this->seedContentSlots();

        $this->seedContentCategories();

        $this->seedContentCategoryItems();
    }
}
