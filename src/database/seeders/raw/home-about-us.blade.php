<div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container py-5">
        <div class="row g-5">
            <div class="col-lg-7">
                <div class="section-title position-relative pb-3 mb-5">
                    <h2 class="fw-bold text-primary text-uppercase">Về chúng tôi</h2>
                    <h4 class="mb-0">Khóa học CNTT tốt nhất với dàn Menter giàu kinh nghiệm.</h4>
                </div>
                <p class="mb-4">
                    Phần này mô tả thông tin sơ lược.
                </p>
                <div class="row g-0 mb-3">
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                        <h6 class="mb-3"><i class="fa fa-check text-primary me-3"></i>Học online & offline.</h5>
                        <h6 class="mb-3"><i class="fa fa-check text-primary me-3"></i>Giáo trình build chuẩn quốc tế.</h6>
                    </div>
                    <div class="col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                        <h6 class="mb-3"><i class="fa fa-check text-primary me-3"></i>Mentor giàu kinh nghiệm tâm huyết.</h6>
                        <h6 class="mb-3"><i class="fa fa-check text-primary me-3"></i>Giá cả hợp lý</h6>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-4 wow fadeIn" data-wow-delay="0.6s">
                    <div class="bg-primary d-flex align-items-center justify-content-center rounded"
                        style="width: 60px; height: 60px;">
                        <i class="fa fa-phone-alt text-white"></i>
                    </div>
                    <div class="ps-4 ml-3">
                        <h5 class="mb-2">Liên hệ ngay với chúng tôi</h5>
                        <h4 class="text-primary mb-0">070 751 9611</h4>
                    </div>
                </div>
                <a href="" class="btn btn-primary py-3 px-5 mt-3 wow zoomIn" data-wow-delay="0.9s">
                    Yêu cầu tư vấn
                </a>
            </div>
            <div class="col-lg-5" style="min-height: 500px;">
                <div class="position-relative h-100">
                    <img class="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s"
                        src="asset('img/about.jpg')" style="object-fit: cover;">
                </div>
            </div>
        </div>
    </div>
</div>
