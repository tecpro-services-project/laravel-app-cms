<div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container py-5">
        <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
            <h2 class="fw-bold text-primary text-uppercase">Khóa học nổi bật</h2>
            <h4 class="mb-0"></h4>
        </div>
        <div class="row g-0">
            @for ($i = 0; $i < 3; $i++)
                <div class="col-12 col-lg-4 px-lg-3" data-wow-delay="0.6s">
                    <div class="course-tile bg-light rounded">
                        <div class="border-bottom mb-4 position-relative">
                            <img class="course-tile__background" src="asset('img/feature.jpg')">
                            <div class="course-tile__actions d-flex bg-primary rounded">
                                <a class="btn text-white" href="#"><span class="badge">Xem thêm</span></a>
                                <a class="btn text-white" href="#"><span class="badge">Tham gia ngay</span></a>
                            </div>
                        </div>
                        <div class="p-4 pt-2">
                            <h3 class="text-center font-weight-normal">
                                {{ number_format(3000000) }}đ
                            </h3>
                            <div class="course-tile__rating d-flex justify-content-center align-items-center mb-3">
                                <i class="fa fa-star text-primary"></i>
                                <i class="fa fa-star text-primary"></i>
                                <i class="fa fa-star text-primary"></i>
                                <i class="fa fa-star text-primary"></i>
                                <i class="fa fa-star text-primary"></i>
                                <span class="pl-2">(123)</span>
                            </div>
                            <p class="text-center font-weight-bold">Website Design & Development Course for Beginners</p>
                        </div>
                        <ul class="list-group-horizontal p-0 d-flex course-tile-footer__action">
                            <li class="list-group-item flex-fill m-0 text-center action__item">
                                <i class="fa fa-user text-primary"></i>
                                <span class="small">
                                    John doe
                                </span>
                            </li>
                            <li class="list-group-item flex-fill m-0 text-center action__item">
                                <i class="fa fa-calendar text-primary"></i>
                                <span class="small">
                                    2 giờ
                                </span>
                            </li>
                            <li class="list-group-item flex-fill m-0 text-center action__item">
                                <i class="fa fa-users text-primary"></i>
                                <span class="small">
                                    15 học viên
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>