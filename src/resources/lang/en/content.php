<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Asset
    |--------------------------------------------------------------------------
    |
    */
    'asset.heading.list.label' => 'Content Asset',
    'asset.heading.form.label' => 'Content Asset Form',
    'asset.list.create.button' => 'New Asset',
    'asset.id.form.label' => 'Asset ID',
    'asset.enable.form.label' => 'Enable',
    'asset.name.form.label' => 'Name',
    'asset.title.form.label' => 'Title',
    'asset.description.form.label' => 'Description',
    'asset.keyword.form.label' => 'Keyword',
    'asset.body.form.label' => 'Body',
    'asset.locale.form.label' => 'Locale',
    'asset.update.form.label' => 'Update',
    'asset.submit.form.label' => 'Create',
    'asset.error.not.found' => 'Content asset not found',
    'asset.error.not.found.with.id' => 'Content asset ":id" not found',
    'list.action.label' => 'Action',
    'list.select.all.label' => 'Select all',

    /*
    |--------------------------------------------------------------------------
    | Content Slot
    |--------------------------------------------------------------------------
    |
    */
    'slot.heading.list.label' => 'Content Slot',
    'slot.list.create.button' => 'New Slot',
    'slot.heading.form.label' => 'Content Slot Form',
    'slot.id.form.label' => 'Slot ID',
    'slot.enable.form.label' => 'Enable',
    'slot.description.form.label' => 'Description',
    'slot.config.form.label' => 'Configuration',
    'slot.update.form.label' => 'Update',
    'slot.submit.form.label' => 'Create',
    'slot.error.not.found.with.id' => 'Content slot ":id" not found',

    /*
    |--------------------------------------------------------------------------
    | Category
    |--------------------------------------------------------------------------
    |
    */
    'category.heading.form.label' => 'Content Category',
    'category.id.form.label' => 'Content Category ID (Note: You can not change the ID after creating the category)',
    'category.enable.form.label' => 'Enable',
    'category.name.form.label' => 'Name',
    'category.title.form.label' => 'Title',
    'category.description.form.label' => 'Description',
    'category.keyword.form.label' => 'Keyword',
    'category.locale.form.label' => 'Locale',
    'category.submit.form.label' => 'Create',
    'category.update.form.label' => 'Update',
    'category.heading.list.label' => 'Category List',
    'category.heading.list.label.has.parent' => 'Category ":parentId"',
    'category.list.create.button' => 'New category',
    'category.list.back.button' => 'Back',
    'category.id.list.label' => 'Category ID',
];
