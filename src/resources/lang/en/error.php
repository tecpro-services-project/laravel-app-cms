<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error message
    |--------------------------------------------------------------------------
    |
    */
    'category.does.not.exist' => 'Category does not exist',
    'category.parent.does.not.exist' => 'Parent category does not exist'
];
