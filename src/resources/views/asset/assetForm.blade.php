@extends('admin::layouts.default')
@section('title', 'Content asset form')
@section('content')

    <div class="page">
        <div class="container pt-4">

            <h1>{{ __('cms::content.asset.heading.form.label') }}</h1>
    
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-important alert-danger alert-dismissible" role="alert">
                        <div class="d-flex">
                            <div>
                                <!-- Download SVG icon from http://tabler-icons.io/i/alert-circle -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24"
                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                    <circle cx="12" cy="12" r="9" />
                                    <line x1="12" y1="8" x2="12" y2="12" />
                                    <line x1="12" y1="16" x2="12.01" y2="16" />
                                </svg>
                            </div>
                            <div>
                                {{ $error }}
                            </div>
                        </div>
                        <a class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="close"></a>
                    </div>
                @endforeach
            @endif
    
            <form class="content-asset-form" action="{{ !isset($contentAsset) ? route('admin.cms.asset.create') : route('admin.cms.asset.update') }}" method="post">
                @csrf
    
                @if (isset($contentAsset) && isset($contentAsset->id))
                    <input type="hidden" name="prevId" value="{{ $contentAsset->id }}">
                @endif
                
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.id.form.label') }}</label>
                    <input type="text" class="form-control" name="id" placeholder="ID" value="{{ $contentAsset->id ?? '' }}">
                </div>
    
                <div class="mb-3">
                    <label class="mb-3 form-check">
                        <input class="form-check-input" type="checkbox" name="is_enable" {{ isset($contentAsset->is_enable) && $contentAsset->is_enable === 1 ? 'checked' : '' }}>
                        <span class="form-check-label">{{ __('cms::content.asset.enable.form.label') }}</span>
                    </label>
                </div>
    
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.name.form.label') }}</label>
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $contentAsset->name ?? '' }}">
                </div>
    
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.title.form.label') }}</label>
                    <input type="text" class="form-control" name="title" placeholder="Title" value="{{ $contentAsset->title ?? '' }}">
                </div>
    
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.description.form.label') }}</label>
                    <input type="text" class="form-control" name="description" placeholder="Description" value="{{ $contentAsset->description ?? '' }}">
                </div>
    
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.keyword.form.label') }}</label>
                    <input type="text" class="form-control" name="keyword" placeholder="Keyword" value="{{ $contentAsset->keyword ?? '' }}">
                </div>
    
                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.asset.body.form.label') }}</label>
                    <textarea class="form-control" name="body" rows="10">{{ $contentAsset->body ?? '' }}</textarea>
                </div>
    
                <div class="mb-3 d-none">
                    <label class="form-label">{{ __('cms::content.asset.locale.form.label') }}</label>
                    <input type="hidden" name="locale_id" value="{{ $contentAsset->locale_id ?? 'vi' }}">
                </div>
    
                <div class="d-flex justify-content-end">
                    <button class="btn btn-primary">
                        {{-- <span class="spinner-border spinner-border-sm me-2" role="status"></span> --}}
                        {{ isset($contentAsset) ? __('cms::content.asset.update.form.label') : __('cms::content.asset.submit.form.label') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
