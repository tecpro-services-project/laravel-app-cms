@extends('admin::layouts.default')
@section('title', 'Content asset')
@section('content')

    <div class="page">
        <div class="page-wrapper">
            <div class="page-header d-print-none">
                <div class="container-xl">
                    <div class="row g-2 align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <h1 class="page-title">
                                {{ __('cms::content.asset.heading.list.label') }}
                            </h1>
                        </div>
                        <!-- Page title actions -->
                        <div class="col-auto ms-auto d-print-none">
                            <div class="btn-list">
                                <span class="d-none d-sm-inline">
                                    <a href="{{ route('admin.cms.asset.form') }}" class="btn">
                                        {{ __('cms::content.asset.list.create.button') }}
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container pt-4 content-asset page__item-list">
                <div class="table-responsive content-asset__list item-list__table">
                    <table class="table table-vcenter">
                        <thead>
                            <tr>
                                <th style="width: 10%" class="text-center"><a href="#">{{ __('cms::content.list.select.all.label') }}</a></th>
                                <th>{{ __('cms::content.asset.id.form.label') }}</th>
                                <th>{{ __('cms::content.asset.name.form.label') }}</th>
                                <th>{{ __('cms::content.list.action.label') }}</th>
                            </tr>
                        </thead>
                        <tbody>
        
                            @foreach ($contentAssetPaging->items() as $contentAsset)
                            <tr>
                                <td class="text-center">
                                    <input class="form-check-input" type="checkbox">
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.asset.form', ['id' => $contentAsset->id]) }}">{{ $contentAsset->id }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.asset.form', ['id' => $contentAsset->id]) }}">{{ $contentAsset->name }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.asset.form', ['id' => $contentAsset->id]) }}">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
        
                <div class="content-asset__paging item-list__paging">
        
                    {{ $contentAssetPaging->render() }}
        
                </div>
        
            </div>
        </div>
    </div>


@endsection
