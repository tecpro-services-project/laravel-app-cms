@extends('admin::layouts.default')
@section('title', $contentAsset->id)
@section('content')

    <div class="page">
        {{!! $contentAsset->body !!}}
    </div>

@endsection
