@extends('admin::layouts.default')
@section('title', 'Content category form')
@section('content')

    <div class="page">
        <div class="container pt-4">

            <h1>{{ __('cms::content.category.heading.form.label') }}</h1>
    
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-important alert-danger alert-dismissible" role="alert">
                        <div class="d-flex">
                            <div>
                                <!-- Download SVG icon from http://tabler-icons.io/i/alert-circle -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24"
                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                    <circle cx="12" cy="12" r="9" />
                                    <line x1="12" y1="8" x2="12" y2="12" />
                                    <line x1="12" y1="16" x2="12.01" y2="16" />
                                </svg>
                            </div>
                            <div>
                                {{ $error }}
                            </div>
                        </div>
                        <a class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="close"></a>
                    </div>
                @endforeach
            @endif
    
            <form class="content-category-form" action="{{ !isset($category) ? route('admin.cms.category.create') : route('admin.cms.category.update') }}" method="post">
                @csrf

                @if (isset($category) && isset($category['id']))
                    <input type="hidden" name="prevId" value="{{ $category['id'] }}">
                @endif

                <div class="mb-3">
                    <label class="form-label">{{ __('cms::content.category.id.form.label') }}</label>
                    <input type="text" class="form-control" name="id" value="{{ $category['id'] ?? '' }}" {{ isset($category) && isset($category['id']) && $category['isAllowUpdateKey'] !== true ? 'readonly ' : ''}}>
                </div>

                <div class="mb-3">
                    <label class="mb-3 form-check">
                        <input class="form-check-input" type="checkbox" name="is_enable" {{ isset($category['detail']['is_enable']) && $category['detail']['is_enable'] === 1 ? 'checked' : '' }}>
                        <span class="form-check-label">{{ __('cms::content.category.enable.form.label') }}</span>
                    </label>
                </div>

                <div class="mb-3 {{ !isset($category) ? 'd-none' : '' }}">
                    <label class="form-label">{{ __('cms::content.category.name.form.label') }}</label>
                    <input type="text" class="form-control" name="name" value="{{ $category['detail']['name'] ?? '' }}">
                </div>
                
                <div class="mb-3 {{ !isset($category) ? 'd-none' : '' }}">
                    <label class="form-label">{{ __('cms::content.category.title.form.label') }}</label>
                    <input type="text" class="form-control" name="title" value="{{ $category['detail']['title'] ?? '' }}">
                </div>
                
                <div class="mb-3 {{ !isset($category) ? 'd-none' : '' }}">
                    <label class="form-label">{{ __('cms::content.category.description.form.label') }}</label>
                    <input type="text" class="form-control" name="description" value="{{ $category['detail']['description'] ?? '' }}">
                </div>
                
                <div class="mb-3 {{ !isset($category) ? 'd-none' : '' }}">
                    <label class="form-label">{{ __('cms::content.category.keyword.form.label') }}</label>
                    <input type="text" class="form-control" name="keyword" value="{{ $category['detail']['keyword'] ?? '' }}">
                </div>

                <div class="mb-3 d-none">
                    <label class="form-label">{{ __('cms::content.category.locale.form.label') }}</label>
                    <input type="hidden" name="locale_id" value="{{ $category['detail']['locale_id'] ?? 'vi' }}">
                </div>

                <div class="mb-3 d-none">
                    <label class="form-label">{{ __('cms::content.category.parent.form.label') }}</label>
                    @if (isset($category['parent_id']) && strcmp($category['parent_id'], '') !== 0)
                        <input type="hidden" name="parent_id" value="{{ $category['parent_id'] }}">
                    @elseif(isset($parentId))
                        <input type="hidden" name="parent_id" value="{{ $parentId }}">
                    @else
                        <input type="hidden" name="parent_id" value="">
                    @endif
                </div>

                <div class="mb-3 d-none">
                    <label class="form-label">{{ __('cms::content.category.path.form.label') }}</label>
                    <input type="hidden" name="path" value="{{ $category['path'] ?? '/' }}">
                </div>

                <div class="d-flex justify-content-end">
                    <button class="btn btn-primary">
                        {{-- <span class="spinner-border spinner-border-sm me-2" role="status"></span> --}}
                        {{ isset($category) ? __('cms::content.category.update.form.label') : __('cms::content.category.submit.form.label') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
