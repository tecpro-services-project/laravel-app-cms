@extends('admin::layouts.default')
@section('title', 'Content category')
@section('content')

    <div class="page">
        <div class="page-wrapper">
            <div class="page-header d-print-none">
                <div class="container-xl">
                    <div class="row g-2 align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <h1 class="page-title">
                                {{ isset($parentId) && strcmp($parentId, '') !== 0
                                ? __('cms::content.category.heading.list.label.has.parent', ['parentId' => $parentId])
                                : __('cms::content.category.heading.list.label') }}
                            </h1>
                        </div>

                        @if (isset($parentId) && strcmp($parentId, '') !== 0)
                            <!-- Back to prev category button -->
                            <div class="col-auto ms-auto d-print-none">
                                <div class="btn-list">
                                    <span class="d-none d-sm-inline">
                                        <a href="{{ route('admin.cms.category.list', ['parentId' => $prevParentId]) }}" class="btn">
                                            {{ __('cms::content.category.list.back.button') }}
                                        </a>
                                    </span>
                                </div>
                            </div>
                        @endif
                        <!-- Create category button -->
                        <div class="col-auto ms-auto d-print-none">
                            <div class="btn-list">
                                <span class="d-none d-sm-inline">
                                    <a href="{{ route('admin.cms.category.form', ['parentId' => $parentId]) }}" class="btn">
                                        {{ __('cms::content.category.list.create.button') }}
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container pt-4 content-asset page__item-list">
                <div class="table-responsive content-asset__list item-list__table">
                    <table class="table table-vcenter">
                        <thead>
                            <tr>
                                <th style="width: 10%" class="text-center"><a href="#">{{ __('cms::content.list.select.all.label') }}</a></th>
                                <th>{{ __('cms::content.category.id.list.label') }}</th>
                                <th>{{ __('cms::content.list.action.label') }}</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($contentCategoryPaging->items() as $contentCategory)
                            <tr>
                                <td class="text-center">
                                    <input class="form-check-input" type="checkbox">
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.category.list', ['parentId' => $contentCategory->id]) }}">{{ $contentCategory->id }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.category.form', ['id' => $contentCategory->id]) }}">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
        
                <div class="content-asset__paging item-list__paging">
        
                    {{ $contentCategoryPaging->render() }}
        
                </div>
        
            </div>
        </div>
    </div>


@endsection
