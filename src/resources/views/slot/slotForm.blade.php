@extends('admin::layouts.default')
@section('title', 'Content slot form')
@section('content')

    <div class="page">
        <div class="page-wrapper">
            <div class="page-header d-print-none">
                <div class="container-xl">
                    <div class="row g-2 align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <h1 class="page-title">
                                {{ __('cms::content.slot.heading.form.label') }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container pt-4">
        
                @include('admin::components.errorMessage')
        
                <form class="content-slot-form" action="{{ !isset($contentSlot) ? route('admin.cms.slot.create') : route('admin.cms.slot.update') }}" method="post">
                    @csrf
        
                    @if (isset($contentSlot) && isset($contentSlot->id))
                        <input type="hidden" name="prevId" value="{{ $contentSlot->id }}">
                    @endif
        
                    <div class="mb-3">
                        <label class="form-label">{{ __('cms::content.slot.id.form.label') }}</label>
                        <input type="text" class="form-control" name="id" placeholder="ID" value="{{ $contentSlot->id ?? '' }}">
                    </div>
        
                    <div class="mb-3">
                        <label class="mb-3 form-check">
                            <input class="form-check-input" type="checkbox" name="is_enable" {{ isset($contentSlot->is_enable) && $contentSlot->is_enable === 1 ? 'checked' : '' }}>
                            <span class="form-check-label">{{ __('cms::content.slot.enable.form.label') }}</span>
                        </label>
                    </div>
        
                    <div class="mb-3">
                        <label class="form-label">{{ __('cms::content.slot.description.form.label') }}</label>
                        <input type="text" class="form-control" name="description" placeholder="Description" value="{{ $contentSlot->description ?? '' }}">
                    </div>
        
                    <div class="mb-3">
                        <label class="form-label">{{ __('cms::content.slot.config.form.label') }}</label>
                        <textarea class="form-control" name="config" rows="10">{{ $contentSlot->config ?? '' }}</textarea>
                    </div>
        
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-primary">
                            {{-- <span class="spinner-border spinner-border-sm me-2" role="status"></span> --}}
                            {{ isset($contentSlot) ? __('cms::content.slot.update.form.label') : __('cms::content.slot.submit.form.label') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
