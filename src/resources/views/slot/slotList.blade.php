@extends('admin::layouts.default')
@section('title', 'Content slot')
@section('content')

    <div class="page">
        <div class="page-wrapper">
            <div class="page-header d-print-none">
                <div class="container-xl">
                    <div class="row g-2 align-items-center">
                        <div class="col">
                            <!-- Page pre-title -->
                            <h2 class="page-title">
                                {{ __('cms::content.slot.heading.list.label') }}
                            </h2>
                        </div>
                        <!-- Page title actions -->
                        <div class="col-auto ms-auto d-print-none">
                            <div class="btn-list">
                                <span class="d-none d-sm-inline">
                                    <a href="{{ route('admin.cms.slot.form') }}" class="btn">
                                        {{ __('cms::content.slot.list.create.button') }}
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container pt-4 content-slot page__item-list">
                <div class="table-responsive content-slot__table item-list__table">
                    <table class="table table-vcenter">
                        <thead>
                            <tr>
                                <th style="width: 10%" class="text-center"><a href="#">{{ __('cms::content.list.select.all.label') }}</a></th>
                                <th>{{ __('cms::content.slot.id.form.label') }}</th>
                                <th>{{ __('cms::content.slot.description.form.label') }}</th>
                                <th>{{ __('cms::content.list.action.label') }}</th>
                            </tr>
                        </thead>
                        <tbody>
        
                            @foreach ($contentSlotPaging->items() as $contentSlot)
                            <tr>
                                <td class="text-center">
                                    <input class="form-check-input" type="checkbox">
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.slot.form', ['id' => $contentSlot->id]) }}">{{ $contentSlot->id }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.slot.form', ['id' => $contentSlot->id]) }}">{{ $contentSlot->description }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.cms.slot.form', ['id' => $contentSlot->id]) }}">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
        
                <div class="content-slot__paging item-list__paging">
        
                    {{ $contentSlotPaging->render() }}
        
                </div>
        
            </div>
        </div>
    </div>


@endsection
