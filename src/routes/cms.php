<?php

use Illuminate\Support\Facades\Route;
use Tecpro\CMS\App\Http\Controllers\ContentCategoryController;
use Tecpro\CMS\App\Http\Controllers\ContentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
| All request will have the prefix /admin/...
|
*/

/**
 * Only authenticated account can access these route
 * 
 * Route namespace prefix will be admin.cms.asset.xxxxx
 * Host prefix will be /admin/cms
 */
Route::name('cms.')
    ->prefix('cms')
    ->middleware('auth:admin')
    ->group(function () {
    /**
     * Host prefix will be /admin/cms/asset/xxxx
     */
    Route::prefix('asset')->group(function () {
        Route::get('/list', [ContentController::class, 'showAssetList'])->name('asset.list');
        Route::get('/form', [ContentController::class, 'showAssetForm'])->name('asset.form');
        Route::post('/create', [ContentController::class, 'createAsset'])->name('asset.create');
        Route::post('/update', [ContentController::class, 'updateAsset'])->name('asset.update');
    });

    /**
     * Host prefix will be /admin/cms/slot/xxxx
     */
    Route::prefix('slot')->group(function () {
        Route::get('/list', [ContentController::class, 'showSlotList'])->name('slot.list');
        Route::get('/form', [ContentController::class, 'showSlotForm'])->name('slot.form');
        Route::post('/create', [ContentController::class, 'createSlot'])->name('slot.create');
        Route::post('/update', [ContentController::class, 'updateSlot'])->name('slot.update');
    });

    /**
     * Host prefix will be /admin/cms/category/xxxx
     */
    Route::prefix('category')->group(function () {
        Route::get('/list', [ContentCategoryController::class, 'showCategoryList'])->name('category.list');
        Route::get('/form', [ContentCategoryController::class, 'showCategoryForm'])->name('category.form');
        Route::post('/create', [ContentCategoryController::class, 'createCategory'])->name('category.create');
        Route::post('/update', [ContentCategoryController::class, 'updateCategory'])->name('category.update');
    });
});
