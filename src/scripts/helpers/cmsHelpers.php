<?php

use Illuminate\Support\Str;
use Tecpro\CMS\Scripts\Managers\Facades\ContentAssetMgr;
use Tecpro\CMS\Scripts\Managers\Facades\ContentSlotMgr;

if (!function_exists('contentCompile')) {
    /**
     * Try to compile the html string follow our rules.
     * @param string $htmlString - The HTML string with some allowed Laravel function
     * @return string The compiled string
     */
    function contentCompile(string $htmlString) {
        $compiledString = $htmlString;

        try {
            $compiledString = preg_replace_callback('/(asset\(\')([^\']*)(\'\))/', function($matches) {
                return asset($matches[2]);
            }, $compiledString);

            $compiledString = preg_replace_callback('/(route\(\')([^\']*)(\'\))/', function($matches) {
                return route($matches[2]);
            }, $compiledString);

            $compiledString = preg_replace_callback('/(url\(\')([^\']*)(\'\))/', function($matches) {
                return url($matches[2]);
            }, $compiledString);
        } catch (Error $error) {
            throw new Error($error->getMessage());
        }

        return $compiledString;
    }
}

/*
|--------------------------------------------------------------------------
| CMS helpers
|--------------------------------------------------------------------------
| These functions will be used on frontpage, not in admin page
*/

if (!function_exists('contentAsset')) {
    /**
     * Get and return the content asset ID
     * @param string $assetId The content asset ID
     * @param string $localeId The locale ID
     * @return string return the HTML content asset body
     */
    function contentAsset(string $assetId, string $localeId) {
        $id = Str::slug($assetId);
        $contentAsset = ContentAssetMgr::get($id, $localeId);

        echo "<!-- Asset ID: $id -->";
        if (!isset($contentAsset)) {
            return __('cms::content.asset.error.not.found.with.id', ['id' => $id]);
        }

        $htmlString = contentCompile($contentAsset->body);

        return $htmlString;
    }
}

if (!function_exists('contentSlot')) {
    /**
     * Render the content slot
     * @param string $slotId The content slot ID
     * @param string $localeId The locale ID
     * @return string return Rendered HTML string
     */
    function contentSlot(string $slotId, string $localeId) {
        $id = Str::slug($slotId);
        $contentSlot = ContentSlotMgr::getAndRender($id, $localeId);
        $htmlString = '';

        echo "<!-- Slot ID: $id -->";
        if (!isset($contentSlot)) {
            return __('cms::content.slot.error.not.found.with.id', ['id' => $id]);
        }

        $contentAssets = $contentSlot['contentAssets'];

        foreach ($contentAssets as $contentAsset) {
            $htmlString .= '<!-- Asset ID: '.$contentAsset['id'].' -->';
            $htmlString .= $contentAsset['body'];
        }

        $htmlString = contentCompile($htmlString);

        return $htmlString;
    }
}