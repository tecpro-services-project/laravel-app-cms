<?php

namespace Tecpro\CMS\Scripts\Managers;

use Tecpro\CMS\App\Models\ContentAsset;
use Tecpro\Core\Scripts\Managers\Abstracts\LocaleMgr;

class ContentAssetMgr extends LocaleMgr
{
    /**
     * Select the content asset based on the given content ID
     * @param string $recordId Content asset ID
     * @param string $localeId The locale ID
     * @return \Tecpro\CMS\App\Models\ContentAsset The content asset model
     */
    public function get(string $recordId, string $localeId)
    {
        return ContentAsset::where('id', '=', $recordId)
            ->where('locale_id', '=', $localeId)
            ->get()->first();
    }

    /**
     * Select the content asset based on the given IDs array
     * @param array $recordIds Content asset ID array
     * @param string $localeId The locale ID
     * @return \Illuminate\Database\Eloquent\Collection The content asset model collecion
     */
    public function getMultiple(array $recordIds, string $localeId)
    {
        // Implode order from array to id1,id2,id3,...
        $recordIdOrdered = trim(implode(',', $recordIds));
        // Replace order to 'id1','id2','id3',... for querying string
        $recordIdOrdered = preg_replace_callback('/([^,]*)/', function ($matches) {
            return "'$matches[0]'";
        }, $recordIdOrdered);
        return ContentAsset::whereIn('id', $recordIds)->where('locale_id', '=', $localeId)->orderByRaw("FIELD(id, $recordIdOrdered)")->get();
    }

    /**
     * Create new content asset
     * @param array $record The content asset data record
     * @return \Tecpro\CMS\App\Models\ContentAsset The created content asset model
     */
    public function create(array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return ContentAsset::create($record);
    }

    /**
     * Update content asset based on the given ID
     * @param string $recordId The content asset ID
     * @param array $record The content asset data record
     * @return \Tecpro\CMS\App\Models\ContentAsset The content asset model which is updated
     */
    public function update(string $recordId, array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return ContentAsset::where('id', '=', $recordId)->update($record);
    }

    /**
     * Delete content asset based on the given ID
     * @param string $recordId The content asset ID
     */
    public function delete(string $recordId)
    {
        ContentAsset::where('id', '=', $recordId)->delete();
    }
}
