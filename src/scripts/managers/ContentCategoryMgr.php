<?php

namespace Tecpro\CMS\Scripts\Managers;

use Tecpro\CMS\App\Models\ContentCategory;
use Tecpro\CMS\App\Models\ContentCategoryItem;
use Tecpro\Core\Scripts\Managers\Abstracts\DefaultMgr;

class ContentCategoryMgr extends DefaultMgr
{
    /**
     * Select the content category based on the given content ID
     * @param string $recordId Content category ID
     * @return \Tecpro\CMS\App\Models\ContentCategory The content category model
     */
    public function get(string $recordId)
    {
        return ContentCategory::where('id', '=', $recordId)->get()->first();
    }

    /**
     * Select the content category based on the given IDs array
     * @param array $ids Content category ID array
     * @param bool $isMaster Determine the collection has only master category or not
     * @return \Illuminate\Database\Eloquent\Collection The content category model collecion
     */
    public function getMultiple(array $recordIds = [], bool $isMaster = false)
    {
        $categories = new ContentCategory();

        // Master category is the category that does not have the parent id
        if ($isMaster) {
            $categories = $categories->where('parent_id', null)->orWhere('parent_id', '');
        }

        if (count($recordIds) === 0) {
            // Get all
            return $categories->get();
        } else {
            // Get categories based on the given record Ids array
            return $categories->whereIn('id', $recordIds)->get();
        }
    }

    /**
     * Create new category
     * @param array $record The content category data record
     * @return \Tecpro\CMS\App\Models\ContentCategory The created content category model
     */
    public function create(array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        $newCategory = new ContentCategory();
        $newCategory->filterCreate($record)->newDetail()->filterCreate($record);
        return $newCategory;
    }

    /**
     * Update content category based on the given ID
     * @param string $recordId The content category ID
     * @param array $record The content category data record
     * @return \Tecpro\CMS\App\Models\ContentCategory The created content category model 
     */
    public function update(string $recordId, array $record)
    {
        $category = $this->get($recordId);
        $record['is_enable'] = $this->isEnable($record);
        $record['path'] = $category->path;
        $category->filterUpdate($record)->getDetailModel($record['locale_id'])->filterUpdate($record);
        return $category;
    }

    /**
     * Delete content category based on the given ID
     * @param string $recordId The content category ID
     */
    public function delete(string $recordId)
    {
        ContentCategory::where('id', '=', $recordId)->delete();
    }

    /**
     * Get sub category of parent category
     * @param ?string $parentId The category parent ID
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator The content category model collecion
     */
    public function getSubCategoryOf(?string $parentId, int $pagingAmount = 0)
    {
        $whereParentId = isset($parentId) ? $parentId : null;
        $result = ContentCategory::where('parent_id', '=', $whereParentId);

        return $pagingAmount === 0
            ? $result->get()
            : $result->paginate($pagingAmount)->withQueryString();
    }

    /**
     * Create content category item
     * @param array $record The content category item
     * @return \Tecpro\CMS\App\Models\ContentCategoryItem The content category item model
     */
    public function createItem(array $record)
    {
        $newItem = new ContentCategoryItem();
        $newItem->filterCreate($record);
        return $newItem;
    }
}
