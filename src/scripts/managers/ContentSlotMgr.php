<?php

namespace Tecpro\CMS\Scripts\Managers;

use Tecpro\CMS\App\Models\ContentSlot;
use Tecpro\CMS\Scripts\Managers\Facades\ContentAssetMgr as ContentAssetMgrFacade;
use Tecpro\Core\Scripts\Managers\Abstracts\DefaultMgr;

class ContentSlotMgr extends DefaultMgr
{
    /**
     * Select the content slot based on the given content ID
     * @param string $recordId content slot ID
     * @return \Tecpro\CMS\App\Models\ContentSlot|null Return content slot model or null
     */
    public function get(string $recordId)
    {
        return ContentSlot::where('id', '=', $recordId)->get()->first();
    }

    /**
     * Select the content slot based on the given IDs array
     * @param array $recordIds content slot ID array
     * @return \Illuminate\Database\Eloquent\Collection The content slot model collecion
     */
    public function getMultiple(array $recordIds)
    {
        return ContentSlot::whereIn('id', $recordIds)->get();
    }

    /**
     * Create new content slot
     * @param array $record The content slot data record
     * @return \Tecpro\CMS\App\Models\ContentSlot The created content slot model
     */
    public function create(array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return ContentSlot::create($record);
    }

    /**
     * Update content slot based on the given ID
     * @param string $recordId The content slot ID
     * @param array $record The content slot data record
     * @return \Tecpro\CMS\App\Models\ContentSlot The content slot model which is updated
     */
    public function update(string $recordId, array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return ContentSlot::where('id', '=', $recordId)->update($record);
    }

    /**
     * Delete content slot based on the given ID
     * @param string $recordId The content slot ID
     */
    public function delete(string $recordId)
    {
        ContentSlot::where('id', '=', $recordId)->delete();
    }

    /**
     * Get and render content in the slot configuration
     * @param string $slotId The content slot ID
     * @param string $localeId The locale ID
     * @return array|null Return slot array or null
     */
    public function getAndRender(string $slotId, string $localeId)
    {
        $contentSlot = $this->get($slotId);

        if (!isset($contentSlot)) return null;

        $slotAssoc = $contentSlot->toArray();
        $slotItems = preg_split("/\r\n|\n|\r/", $slotAssoc['config']);
        $contentAssets = ContentAssetMgrFacade::getMultiple($slotItems, $localeId);
        $slotAssoc['contentAssets'] = $contentAssets->toArray();

        return $slotAssoc;
    }
}
