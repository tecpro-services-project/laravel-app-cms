<?php

namespace Tecpro\CMS\Scripts\Managers\Facades;

use Illuminate\Support\Facades\Facade;

class ContentCategoryMgr extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'contentCategoryMgr';
    }
}